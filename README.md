# Android Problems and Solutions

### Can not connect to device (e. g. device has no serial number)

1. restart in USB mode
```
adb usb
```
2. List atached devices
```
adb devices
```
3. If serial number is empty or ?? or else: restart in TCP mode port: 5555
```
adb tcpip 5555
```
4. Now find the phone's IP address via Settings -> About -> Status -> IP address.
5. connected to YOUR_IP_ADDRESS:5555
```
adb connect YOUR_IP_ADDRESS
```
6. Now your device should be listed and you can use it
```
adb devices
```
7. When finished, close the connection
```
adb disconnect YOUR_IP_ADDRESS
```

-----
